class CreateTwitters < ActiveRecord::Migration
  def change
    create_table :twitters do |t|
      t.string :name
      t.string :tweet
      t.string :image

      t.timestamps null: false
    end
  end
end
