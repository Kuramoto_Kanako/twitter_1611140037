require 'test_helper'

class TwittersControllerTest < ActionController::TestCase
  #show ----------------------------------------------
  test "twitterを1つ表示すること" do
    get :show, id: 1
    assert_response :success
    assert_not_nil assigns(:twitter)
    assert_template "show"
  end
  
  test "twitterを1つ表示するHTMLの構造が適切か" do
    get :show, id: 1
    assert_response :success
      assert_select "ul.click_menu_checkbox"
      assert_select "a", "ツイートを削除"
      assert_select "div.posts-show-item2"
  end
  
  #index ----------------------------------------------
  test "twitterを全て表示すること" do
    get :index
    assert_response :success
    assert_not_nil assigns(:twitters)
    assert_template "index"
  end
  
  test "twitterを全て表示するHTMLの構造が適切か" do
    get :index
    assert_response :success
      assert_select "a", "ツイート"
      assert_select "div.posts-index-item"
      assert_select "div.posts-show-item"
  end
  
  
  #create ----------------------------------------------
  test "twitterをcreateにより、Twitterのレコード数が1増加し、一覧画面に遷移する" do
    assert_difference('Twitter.count', 1) do
      post :create, twitter: {name: "かなこ", tweet: "ありがとう"}
    end 
    assert_not_nil Twitter.find_by(name: "かなこ")
    assert_response :redirect
    assert_redirected_to '/'
  end
  
  #update ----------------------------------------------
  test "twitterを更新すること" do
    twitter = Twitter.new
    patch :update, id: 1, twitter: {name: "ハナコ", tweet: "ごめんね"}
    assert_response :redirect
    twitter = Twitter.find_by(id: 1)
    assert_not_nil twitter.name, "ハナコ"
    assert_redirected_to '/'
  end
  
  #delete ----------------------------------------------
  test "twitterを削除することにより、Twitterのレコード数が1減少し、一覧画面に遷移する" do
    assert_difference('Twitter.count', -1) do
      get :destroy, id: 1
    end
    assert_nil Twitter.find_by(id: 1)
    assert_response :redirect
    assert_redirected_to '/'
  end

end
