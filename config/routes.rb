Rails.application.routes.draw do
  resources :twitters
  post 'twitters/create'
  post 'twitters/search'
  post 'twitters/update'
  root 'twitters#index'
  get '/media' => 'twitters#media'
end
