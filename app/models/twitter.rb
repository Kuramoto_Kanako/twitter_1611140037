class Twitter < ActiveRecord::Base
  
  #ツイートの文字数は140文字以内で空ではない
  validates :tweet, length: {in: 1..140},
                    presence: true
                     
  #ユーザー名は10文字以内で空ではない
  validates :name, length: {in: 1..10},
                   presence: true
  
  mount_uploader :image, ImageUploader
end

