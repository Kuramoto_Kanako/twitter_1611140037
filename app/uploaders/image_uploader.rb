class ImageUploader < CarrierWave::Uploader::Base
  # 拡張子 jpg jpeg gif png のみ許可
  def extension_white_list
    %w(jpg jpeg gif png)
  end
  
  # ファイル名はオリジナルの名前で保存
  def filename
    original_filename + '.jpg' if original_filename
  end
  
  # 画像を保存する場所
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end
end
