class TwittersController < ApplicationController
  def index
    @twitters = Twitter.all.order(created_at: :desc)
    @twitter = Twitter.new
    @count = @twitters.count
  end
  
  def post_params
    params.require(:twitter).permit(:name, :tweet, :image)
  end

  def search
    @twitters = Twitter.all.order(created_at: :desc)
    @twitter = Twitter.new
    @twitters = Twitter.where(['name LIKE ?', "%#{params[:keyword]}%"]).order(created_at: :desc)
    @count = @twitters.count
    render "index"
  end
  
  def create
    @twitter = Twitter.new(params.require(:twitter).permit(:name, :tweet, :image))
    if @twitter.save
      redirect_to root_path
    else
      render 'index'
    end
  end
  
  def show
    @twitters = Twitter.all.order(created_at: :desc)
    @twitter = Twitter.new
    @twitter_u = Twitter.find(params[:id])
    @count = @twitters.count
  end
  
  def media
    @twitters = Twitter.all.order(created_at: :desc)
    @twitter = Twitter.new
    @count = @twitters.count
    if @twitter.image.present?
      @twitters = Twitter.order(created_at: :desc)
    end
  end
  
  def update
    @twitter_u = Twitter.find(params[:id])
    @twitter_u.update(params.require(:twitter).permit(:name, :tweet, :image))
    redirect_to root_path
  end
  
  def destroy
    twitter = Twitter.find(params[:id])
    twitter.destroy
    redirect_to root_path
  end
end
